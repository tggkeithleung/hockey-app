angular
  .module('groupmembership')
  .controller("EditController", function ($scope, Groupmembership, supersonic) {
    $scope.groupmembership = null;
    $scope.showSpinner = true;

    // Fetch an object based on id from the database
    Groupmembership.find(steroids.view.params.id).then( function (groupmembership) {
      $scope.$apply(function() {
        $scope.groupmembership = groupmembership;
        $scope.showSpinner = false;
      });
    });

    $scope.submitForm = function() {
      $scope.showSpinner = true;
      $scope.groupmembership.save().then( function () {
        supersonic.ui.modal.hide();
      });
    }

    $scope.cancel = function () {
      supersonic.ui.modal.hide();
    }

  });
