angular
  .module('groupmembership')
  .controller("NewController", function ($scope, Groupmembership, supersonic) {
    $scope.groupmembership = {};

    $scope.submitForm = function () {
      $scope.showSpinner = true;
      newgroupmembership = new Groupmembership($scope.groupmembership);
      newgroupmembership.save().then( function () {
        supersonic.ui.modal.hide();
      });
    };

    $scope.cancel = function () {
      supersonic.ui.modal.hide();
    }

  });