angular
  .module('groupmembership')
  .controller("IndexController", function ($scope, Groupmembership, supersonic) {
    $scope.groupmemberships = null;
    $scope.showSpinner = true;

    $scope.back = function (){
      supersonic.ui.modal.hide();
    }

    Groupmembership.all().whenChanged( function (groupmemberships) {
        $scope.$apply( function () {
          $scope.groupmemberships = groupmemberships;
          $scope.showSpinner = false;
        });
    });
  });