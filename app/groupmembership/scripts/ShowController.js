angular
  .module('groupmembership')
  .controller("ShowController", function ($scope, Groupmembership, supersonic) {
    $scope.groupmembership = null;
    $scope.showSpinner = true;
    $scope.dataId = undefined;

    var _refreshViewData = function () {
      Groupmembership.find($scope.dataId).then( function (groupmembership) {
        $scope.$apply( function () {
          $scope.groupmembership = groupmembership;
          $scope.showSpinner = false;
        });
      });
    }

    supersonic.ui.views.current.whenVisible( function () {
      if ( $scope.dataId ) {
        _refreshViewData();
      }
    });

    supersonic.ui.views.current.params.onValue( function (values) {
      $scope.dataId = values.id;
      _refreshViewData();
    });

    $scope.remove = function (id) {
      $scope.showSpinner = true;
      $scope.groupmembership.delete().then( function () {
        supersonic.ui.layers.pop();
      });
    }
  });