angular
  .module('contact')
  .controller('IndexController', function($scope, supersonic) {
    // Controller functionality here

    $scope.contacts = null;

  	function onSuccess(contacts) {
    	//alert('Found ' + contacts.length + ' contacts.');

    	//var contacts = this;
    	$scope.contacts = contacts;
    	supersonic.logger.log($scope.contacts);
	};

	function onError(contactError) {
	    alert('onError!');
	};

	//find all contacts with 'Bob' in any name field
	var options      = new ContactFindOptions();
	options.filter   = "sir";
	options.multiple = true;
	var fields       = ["displayName"];
	navigator.contacts.find(fields, onSuccess, onError, options);

	

  });
