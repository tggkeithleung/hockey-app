angular
  .module('event')
  .controller("IndexController", function ($scope, Event, supersonic) {
    $scope.events = null;
    $scope.showSpinner = true;

    // var options = {
    //   title:'abcabc',
    //   onTap: function(){
    //     supersonic.ui.drawers.open('left')
    //   },
    //   styleId:"nav__menu",
    //   //styleCSS: "background-images:url:('/icons/round.svg')",
      
    // };

    // var leftButton = new supersonic.ui.NavigationBarButton(options);
    // supersonic.ui.navigationBar.update({
    //   overrideBackButton: true,
    //   buttons:{
    //     left: [leftButton]
    //   }
    // });

    //var myContact = navigator.contacts.find({"displayName": "Test User"});
    //supersonic.logger.error(myContact);
    supersonic.ui.modal.show('register#index');
    $scope.createNewEvent = function(){
     
      supersonic.ui.modal.show('event#new');
    }
    $scope.openDrawer = function(){
    //supersonic.ui.modal.show('event#new');
    supersonic.ui.drawers.open("left")
    }
    Event.all().whenChanged( function (events) {
        $scope.$apply( function () {
          $scope.events = events;
          $scope.showSpinner = false;
        });
    });
  });