angular
  .module('contactlist')
  .controller("NewController", function ($scope, Contactlist, supersonic) {
    $scope.contactlist = {};

    $scope.submitForm = function () {
      $scope.showSpinner = true;
      newcontactlist = new Contactlist($scope.contactlist);
      newcontactlist.save().then( function () {
        supersonic.ui.modal.hide();
      });
    };

    $scope.cancel = function () {
      supersonic.ui.modal.hide();
    }

  });