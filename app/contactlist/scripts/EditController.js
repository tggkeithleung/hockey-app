angular
  .module('contactlist')
  .controller("EditController", function ($scope, Contactlist, supersonic) {
    $scope.contactlist = null;
    $scope.showSpinner = true;

    // Fetch an object based on id from the database
    Contactlist.find(steroids.view.params.id).then( function (contactlist) {
      $scope.$apply(function() {
        $scope.contactlist = contactlist;
        $scope.showSpinner = false;
      });
    });

    $scope.submitForm = function() {
      $scope.showSpinner = true;
      $scope.contactlist.save().then( function () {
        supersonic.ui.modal.hide();
      });
    }

    $scope.cancel = function () {
      supersonic.ui.modal.hide();
    }

  });
