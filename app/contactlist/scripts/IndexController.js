angular
  .module('contactlist')
  .controller("IndexController", function ($scope, Contactlist, supersonic) {
    $scope.contactlists = null;
    $scope.showSpinner = true;

    Contactlist.all().whenChanged( function (contactlists) {
        $scope.$apply( function () {
          $scope.contactlists = contactlists;
          $scope.showSpinner = false;
        });
    });

    $scope.back = function(){
      supersonic.ui.modal.hide();
    }
  });