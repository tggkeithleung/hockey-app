angular
  .module('contactlist')
  .controller("ShowController", function ($scope, Contactlist, supersonic) {
    $scope.contactlist = null;
    $scope.showSpinner = true;
    $scope.dataId = undefined;

    var _refreshViewData = function () {
      Contactlist.find($scope.dataId).then( function (contactlist) {
        $scope.$apply( function () {
          $scope.contactlist = contactlist;
          $scope.showSpinner = false;
        });
      });
    }

    supersonic.ui.views.current.whenVisible( function () {
      if ( $scope.dataId ) {
        _refreshViewData();
      }
    });

    supersonic.ui.views.current.params.onValue( function (values) {
      $scope.dataId = values.id;
      _refreshViewData();
    });

    $scope.remove = function (id) {
      $scope.showSpinner = true;
      $scope.contactlist.delete().then( function () {
        supersonic.ui.layers.pop();
      });
    }
  });