angular
  .module('group')
  .controller("IndexController", function ($scope, Group, supersonic) {
    $scope.groups = null;
    $scope.showSpinner = true;

    Group.all().whenChanged( function (groups) {
        $scope.$apply( function () {
          $scope.groups = groups;
          $scope.showSpinner = false;
        });
    });
  });