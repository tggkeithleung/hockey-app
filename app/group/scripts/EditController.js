angular
  .module('group')
  .controller("EditController", function ($scope, Group, supersonic) {
    $scope.group = null;
    $scope.showSpinner = true;

    // Fetch an object based on id from the database
    Group.find(steroids.view.params.id).then( function (group) {
      $scope.$apply(function() {
        $scope.group = group;
        $scope.showSpinner = false;
      });
    });

    $scope.submitForm = function() {
      $scope.showSpinner = true;
      $scope.group.save().then( function () {
        supersonic.ui.modal.hide();
      });
    }

    $scope.cancel = function () {
      supersonic.ui.modal.hide();
    }

  });
