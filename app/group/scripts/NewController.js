angular
  .module('group')
  .controller("NewController", function ($scope, Group, supersonic) {
    $scope.group = {};

    $scope.submitForm = function () {
      $scope.showSpinner = true;
      $scope.group.is_public = true;
      $scope.group.create_by_id = 1;
      $scope.group.create_date = Date.now();

      newgroup = new Group($scope.group);
      newgroup.save().then( function () {
        supersonic.ui.modal.hide();
      });
    };

    $scope.cancel = function () {
      supersonic.ui.modal.hide();
    }

  });