angular
  .module('group')
  .controller("ShowController", function ($scope, Group, supersonic) {
    $scope.group = null;
    $scope.contacts = null;
    $scope.showSpinner = true;
    $scope.dataId = undefined;

    var _refreshViewData = function () {
      Group.find($scope.dataId).then( function (group) {
        $scope.$apply( function () {
          $scope.group = group;
          $scope.showSpinner = false;
        });
      });
    }

    supersonic.ui.views.current.whenVisible( function () {
      if ( $scope.dataId ) {
        _refreshViewData();
      }
    });

    supersonic.ui.views.current.params.onValue( function (values) {
      $scope.dataId = values.id;
      _refreshViewData();
    });

    $scope.remove = function (id) {
      $scope.showSpinner = true;
      $scope.group.delete().then( function () {
        supersonic.ui.layers.pop();
      });
    }

    // function onSuccess(contacts) {
    //   //alert('Found ' + contacts.length + ' contacts.');

    //   //var contacts = this;
    //   $scope.contacts = contacts;
    //   supersonic.logger.log($scope.contacts);
    // };

    // function onError(contactError) {
    //     alert('onError!');
    // };

    // //find all contacts with 'Bob' in any name field
    // var options      = new ContactFindOptions();
    // options.filter   = "sir";
    // options.multiple = true;
    // var fields       = ["displayName"];
    // navigator.contacts.find(fields, onSuccess, onError, options);

    });