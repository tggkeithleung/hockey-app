angular
  .module('register')
  .controller('ConfirmController', function($scope, supersonic) {
    // Controller functionality here
    	$scope.cancel = function (){
    		supersonic.ui.navigationBar.hide();
    	}
    	$scope.validate = function (){
    		var view = new supersonic.ui.View('register#register');
    		supersonic.ui.layers.push(view);
    	}
  });

