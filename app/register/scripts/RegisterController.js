angular
  .module('register')
  .controller("RegisterController", function ($scope, supersonic) {
    

    $scope.submitForm = function () {
      var options = {
        message: "Your registration was successful. Please continue to the Hockey Schedule App.",
        buttonLabel: "OK"
      };

      supersonic.ui.dialog.alert("WELCOME", options).then(function() {
        
      });
      supersonic.ui.modal.hideAll();
    };

  });