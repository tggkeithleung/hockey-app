# Read more about app structure at http://docs.appgyver.com

module.exports =

  # See styling options for tabs and other native components in app/common/native-styles/ios.css or app/common/native-styles/android.css
 

  rootView:
    location: "event#index"

  preloads: [
    {
      id: "learn-more"
      location: "example#learn-more"
    }
    {
      id: "using-the-scanner"
      location: "example#using-the-scanner"
    }
    {
      id: ""
      location: "example#using-the-scanner"
    }
  ]

  drawers:
   left:
     id: "leftDrawer"
     location: "drawers#index"
     showOnAppLoad: false
   options:
     animation: "swingingDoor"

 initialView:
   id: "initialView"
   location: "example#initial-view"
